import cv2
import glob
import ntpath
import multiprocessing
import sys


def func(path, out_path, threshold):
    for file in glob.glob(path):
        img = cv2.imread(file)
        img_grayscale = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img_resized = cv2.resize(img_grayscale, (100, 100))
        rows, cols = img_resized.shape
        v = 0
        for i in range(rows):
            for j in range(cols):
                v += img_resized[i, j]
        v = v / (rows * cols)
        v = round((255 - v) / 255 * 100)

        if v > threshold:
            result = 'dark'
        else:
            result = 'bright'

        file_name = ntpath.basename(file).rsplit('.')[0]
        name = out_path + file_name + '_' + result + '_' + str(int(v)) + '.jpg'
        cv2.imwrite(name, img)


def main():
    if len(sys.argv) == 1:
        threshold = 80
    elif len(sys.argv) == 2:
        try:
            threshold = int(sys.argv[1])
        except ValueError:
            print("Could not convert data to an integer.")
            sys.exit()
    else:
        print(f"Too many arguments. Expected 1, got {len(sys.argv) - 1}")
        sys.exit()
    config_path = "config.txt"
    lines = [line.rstrip('\n') for line in open(config_path)]
    in_path = lines[0]
    out_path = lines[1]

    path_list = ['{}*.jpg'.format(in_path), '{}*.jpeg'.format(in_path), '{}*.png'.format(in_path)]
    processes = []

    for path in path_list:
        p = multiprocessing.Process(target=func, args=(path, out_path, threshold))
        processes.append(p)
        p.start()

    for p in processes:
        p.join()


if __name__ == '__main__':
    main()
