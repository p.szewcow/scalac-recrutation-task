import java.awt.image.BufferedImage
import java.awt.Graphics
import java.awt.Image
import java.io.File
import javax.imageio.ImageIO
import scala.io.Source


object AYAotD {
	def main(args: Array[String]){
		def toGray(img: BufferedImage): BufferedImage = {
			val w = img.getWidth
			val h = img.getHeight
			
			val out = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_GRAY)
			
			val g = out.getGraphics
			g.drawImage(img, 0, 0, null)
			g.dispose()
			
			out
		}
		
		def resize(img: BufferedImage, w: Int, h: Int): BufferedImage = {
			val r = img.getScaledInstance(w, h, Image.SCALE_DEFAULT)
			
			val out = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_GRAY)
			
			val g = out.getGraphics
			g.drawImage(r, 0, 0, null)
			g.dispose()
			
			out
		}
		
		def getListOfFiles(dir: String): List[File] = {
			val d = new File(dir)
			if (d.exists && d.isDirectory) {
				d.listFiles.filter(_.isFile).toList
			} else {
				List[File]()
			}
		}
		
		var threshold = 80
		if(args.length == 1){
			threshold = args(0).toInt
		} else if (args.length > 1){
			println("Too many arguments. Expected 1, got " + args.length + ".")
			return
		}
		println(threshold)
		
		val lines = Source.fromFile("config.txt").getLines.toList
		val in = lines(0)
		val out = lines(1)
		
		val directories = getListOfFiles(in)
		
		for(i <- directories) {
			val name: String = i.getName
			val jpgRemoved: String = name.split("\\.")(0)
			val img: BufferedImage = ImageIO.read(i)
		
			val gray = toGray(img)
			val resized = resize(img, 100, 100)

		
			var brightness: Float = 0
			for(j <- 0 until resized.getWidth; k <- 0 until resized.getHeight){
				brightness += resized.getData().getSample(j, k, 0)
			}
		
			brightness /= resized.getWidth * resized.getHeight
			brightness /= 255
			brightness = Math.round(100 - brightness * 100)
		
			var text = ""
			if (brightness > threshold){
				text = "dark"
			}
			else{
				text = "bright"
			}
		
			ImageIO.write(img, "jpg", new File(out + jpgRemoved + "_" + text + "_" + brightness + ".jpg"))
		}
	}
}