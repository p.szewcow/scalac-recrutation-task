"Are you afraid of the dark"
This is recrutation task for apprenticeships in Scalac by Paweł Szewców.

Task will be done in Python at first because it is my main language at the
moment then I will translate it to Scala.

# In Python
In Python console type:
`pip install -r requirements.txt`.

You can run script using:
`python task.py`
or:
`python task.py (threshold)`.

Defaul value of threshold is 80. If you enter value under 0 or over 100 the code
will still work.

If you want to change in/out directories go to `config.txt`.

# In Scala
To run program in Scala console type:
`scala task.scala`
or:
`scala task.scala (threshold)`

Defaul value of threshold is 80. If you enter value under 0 or over 100 the code
will still work.

If you want to change in/out directories go to `config.txt`.

# Details
Score is evaluated as:

$`score=\frac{255 - v}{255}*100`$

where:

$`v = \frac{\sum_{x=0}^{W-1}\sum_{y=0}^{H-1}v_{x, y}}{W*H}`$

where:

$`v_{x, y}`$ - value of x-th, y-th pixel in grayscale image,

$`W`$ - width of an image,

$`H`$ - height of an image.